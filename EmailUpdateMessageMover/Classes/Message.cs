﻿using Limilabs.Client.IMAP;
using Limilabs.Mail;
using Limilabs.Mail.MIME;
using ProcuctDB;
using ProcuctDB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    public class Message
    {
        public static void MoveToPBSEasyFolder(IEmail email)
        {
            Imap imap = email.GetImap();

            foreach (long uid in email.GetUids())
            {
                IMail mail = new MailBuilder().CreateFromEml(imap.GetMessageByUID(uid));

                try 
                { 
                    MimeData mime = mail.Attachments.First(); 

                    ProductFinder productFinder = new ProductFinder();
                    productFinder.GetPBSUpdaters().ForEach(supplierEmailDownloadInfo =>
                    {
                        if (mime.FileName.Contains(supplierEmailDownloadInfo.getKipFileName()) ||
                            mime.FileName.Contains(supplierEmailDownloadInfo.getProdFileName()) ||
                            mime.FileName.Contains(supplierEmailDownloadInfo.getProd2FileName()))
                        {
                            imap.MoveByUID(uid, "PBSEasy");
                            Console.WriteLine(string.Format("{0} message was moved to PBSEasy folder", uid));
                        }
                    });
                }
                catch { }
            }

            imap.Dispose();
        }
    }
}
