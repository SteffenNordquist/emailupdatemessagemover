﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    public class Credentials: ICredentials
    {
        private readonly string email = "";
        private readonly string password = "";

        public Credentials()
        {
            this.email = Properties.Settings.Default.email;
            this.password = Properties.Settings.Default.password;
        }

        public string GetEmail()
        {
            return email;
        }

        public string GetPassword()
        {
            return password;
        }
    }
}
