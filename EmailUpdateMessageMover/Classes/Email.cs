﻿using Limilabs.Client.IMAP;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    public class Email: IEmail
    {
        private static Imap imap = null;
        private readonly ICredentials credentials = null;
        private List<long> uids = new List<long>();

        public Email(ICredentials credentials)
        {
            this.credentials = credentials;  
        }

        public void Connect()
        {
            imap = new Imap();
            imap.ConnectSSL("imap.gmail.com"/*, 993*/);
            imap.Login(credentials.GetEmail(), credentials.GetPassword());
            imap.SelectInbox();
        }

        public Imap GetImap()
        {
            return imap;
        }

        public List<long> GetUids()
        {
            uids = imap.Search(Limilabs.Client.IMAP.Flag.All);
            uids.Reverse();
            //uids.GetRange(0, 15);
            return uids;
        }


    }
}
