﻿using DN_Classes.Entities.Supplier;
using DN_Classes.Supplier;
using ProcuctDB;
using ProcuctDB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    class Program
    {
        static void Main(string[] args)
        {
            IEmail email = new Email(new Credentials());
            email.Connect();

            Message.MoveToPBSEasyFolder(email);
        }
    }
}
