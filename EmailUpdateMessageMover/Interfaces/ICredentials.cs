﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    public interface ICredentials
    {
        string GetEmail();
        string GetPassword();
    }
}
