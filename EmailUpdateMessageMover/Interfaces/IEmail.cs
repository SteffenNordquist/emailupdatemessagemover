﻿using Limilabs.Client.IMAP;
using ProcuctDB.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EmailUpdateMessageMover
{
    public interface IEmail
    {
        void Connect();
        Imap GetImap();
        List<long> GetUids();
    }
}
